package AI;

import java.util.ArrayList;

import AI.GrapthUtil;
import AI.Node;
import javafx.scene.control.TextArea;

public class SearchCheapestPath implements ISearch {

	private Graph graph;
	private TextArea textArea;

	public SearchCheapestPath(Graph graph, TextArea textArea) {
		this.graph = graph;
		this.textArea = textArea;
	}

	@Override
	public ArrayList<String> hasPath(String start, String end) {
		if (!graph.containsNode(start) || !graph.containsNode(start)) {
			return null;
		}

		graph.resetAll();

		ArrayList<Node> queue = new ArrayList<>();
		Node temp;
		queue.add(graph.getNode(start));
		while (!queue.isEmpty()) {
			temp = queue.get(0);
			GrapthUtil.printNodeInfo(temp, textArea);
			queue.remove(0);
			temp.tested = true;
			if (!temp.expanded) {
				for (Link link : temp.listLinks) {
					Node rNode = link.relatedNode;
					GrapthUtil.setParentByPrice(temp, link);
					queue.add(rNode);
					queue = GrapthUtil.sortQueueByDistance(queue);
				}
				temp.expanded = true;
			}
		} // end while for queue

		if (graph.getNode(end).parent == null) {
			return null;
		} else {
			GrapthUtil.printKMandPrice(graph, end, textArea);
			ArrayList<String> path = new ArrayList<>();
			GrapthUtil.printPath(graph.getNode(start), graph.getNode(end), path);
			return path;
			// GrapthUtil.printPathByParent(graph, end);
		}
	}
}
