package AI;

import java.util.ArrayList;

import AI.Graph;
import AI.GrapthUtil;
import AI.ISearch;
import AI.Node;
import javafx.scene.control.TextArea;

public class breathSearch implements ISearch {

	private Graph graph;
	private TextArea textArea;

	public breathSearch(Graph graph, TextArea textArea) {
		this.graph = graph;
		this.textArea = textArea;
	}

	@Override
	public ArrayList<String> hasPath(String start, String end) {
		graph.resetAll();
		if (!graph.containsNode(start) || !graph.containsNode(end)) {
			return null;
		}

		ArrayList<Node> queue = new ArrayList<>();
		Node temp;
		queue.add(graph.getNode(start));

		while (!queue.isEmpty()) {
			temp = queue.get(0);
			GrapthUtil.printNodeInfo(temp, textArea);
			queue.remove(0);
			temp.tested = true;
			if (temp.name.equals(end)) {
				GrapthUtil.printKMandPrice(graph, end, textArea);
				ArrayList<String> path = new ArrayList<>();
				GrapthUtil.printPath(graph.getNode(start), graph.getNode(end), path);
				return path;
			}
			if (!temp.expanded) {
				for (Link link : temp.listLinks) {
					Node rNode = link.relatedNode;
					if (!link.relatedNode.tested) {
						GrapthUtil.setParentByPrice(temp, link);
						rNode.tested = true;
						rNode.parent = temp;
						queue.add(rNode);
						//queue.add(0, node);
					}
				}
				temp.expanded = true;
			}
		} // end while
		
		return null;

	}// end hasPath

	public void printQueue(ArrayList<Node> arr) {
		for (Node node : arr) {
			System.out.print(node.name + ", ");
		}
	}
}