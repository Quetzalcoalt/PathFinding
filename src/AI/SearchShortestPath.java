package AI;

import java.util.ArrayList;

import AI.GrapthUtil;
import AI.Node;
import javafx.scene.control.TextArea;

public class SearchShortestPath implements ISearch {

	private Graph graph;
	private TextArea textArea;

	public SearchShortestPath(Graph graph, TextArea textArea) {
		this.graph = graph;
		this.textArea = textArea;
	}

	@Override
	public ArrayList<String> hasPath(String start, String end) {
		if (!graph.containsNode(start) || !graph.containsNode(end)) {
			return null;
		}
		graph.resetAll();

		Node temp = graph.getNode(start);
		temp.km = 0;
		ArrayList<Link> queue = new ArrayList<>();
		queue.addAll(temp.listLinks);
		while (!queue.isEmpty()) {
			System.out.println(temp.name);
			System.out.print("[");
			for (int i = 0; i < queue.size(); i++) {
				System.out.print(queue.get(i).relatedNode.name + ", ");
			}
			System.out.print("]\n");
			GrapthUtil.printNodeInfo(temp, textArea);
			for (Link link : queue) {
				GrapthUtil.setParentCostAndPrice(temp, link);
			}

			temp.tested = true;
			queue.clear();
			// nai malkoto ot vsi4ko v grapha
			Node minNode = GrapthUtil.findLowestKMInGraph(graph);
			if (minNode != null) {
				for (Link link : minNode.listLinks) {
					queue.add(link);
				}
				temp = minNode;
			}
		}

		if (graph.getNode(end).parent == null) {
			return null;
		} else {
			GrapthUtil.printKMandPrice(graph, end, textArea);
			ArrayList<String> path = new ArrayList<>();
			GrapthUtil.printPath(graph.getNode(start), graph.getNode(end), path);
			return path;
			// GrapthUtil.printPathByParent(graph, end);
		}
	}
}
