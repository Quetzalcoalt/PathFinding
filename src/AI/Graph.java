package AI;

import java.util.HashMap;

import UI.MouseGestures;
import javafx.scene.control.ContextMenu;
import javafx.stage.Stage;
import AI.Node;

public class Graph {

	MouseGestures mouseGestures = new MouseGestures(this);

	private HashMap<String, Node> map = new HashMap<String, Node>();

	public void addNode(Node node) {
		map.put(node.name, node);
	}
	
	public void oneWayLink(String start, String end, double length, int roadType){
		if(map.containsKey(start) && map.containsKey(end)){
			//map.get(start).links.add(map.get(end));
			Link link = new Link(map.get(end), length, roadType);
			link.setPrice(GrapthUtil.calculatePrice(link));
			map.get(start).listLinks.add(link);
			//System.out.println(start + ", " + link.relatedNode.name + " price is: " + link.price);
		}else{
			System.out.println("Missing nodes");
		}
	}
	
	public void twoWayLink(String start, String end, double length, int roadType){
		oneWayLink(start, end, length, roadType);
		oneWayLink(end, start, length, roadType);
	}

	public void resetAll() {
		for (Node node : map.values()) {
			node.reset();
		}
	}
	
	public void clearAll(){
		map.clear();
	}
	
	public boolean containsNode(String name){
		return map.containsKey(name);
	}
	
	public Node getNode(String name){
		return map.get(name);
	}

	public HashMap<String, Node> getMap() {
		return map;
	}

	public void makeDragable() {
		for (Node node : map.values()) {
			mouseGestures.makeDraggable(node);
		}
	}
	
	public void makeDragable(Node node){
		mouseGestures.makeDraggable(node);
	}
	
	public void makeClicable(ContextMenu menu, Stage stage) {
		for (Node node : map.values()) {
			mouseGestures.makeClickable(node, menu, stage);
		}
	}

	public void makeClicable(Node node, ContextMenu menu, Stage stage) {
		mouseGestures.makeClickable(node, menu, stage);
	}

	
	public void deleteNode(Node node) {
		for(Node nodes : map.values()){
			for(Link nodesLinks: nodes.listLinks){
				map.get(nodesLinks.relatedNode.name).removeLink(node.name);
			}
		}
		map.remove(node.name);
	}
	
	public void printAllLinks(){
		for(Node node: map.values()){
			System.out.println("Node " + node.name + " has: ");
			for(Link links : node.listLinks){
				System.out.println("Link: " + links.relatedNode.name);
			}
		}
	}
}
