package UI;

import AI.Link;
import AI.Node;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;

public class Connection extends Group {

	Line line;
	Text lineLength;
	Line leftArrow;
	Line rightArrow;
	Text weight;
	Arrow arrow;
	Boolean isDown;

	public Connection(Node parent, Link child) {

		Node temp = child.relatedNode;
		line = new Line();
		line.setStrokeWidth(3);
		arrow = new Arrow();
		weight = new Text("" + parent.weight);
		lineLength = new Text("" + child.pathLenght + " Type: " + child.roadType);
		lineLength.setStroke(Color.DARKORCHID);
		getChildren().addAll(line, arrow, lineLength, weight);
		// lineLength.setText("" + Math.round(getDiagonal(parent, child)));
		// parent.linkLength.replace(child, getDiagonal(parent, child));
		// lineLength.setText("" + parent.linkLength.get(child));

		// if (parent.links.contains(child) && child.links.contains(parent)) {
		if (parent.listLinks.contains(child) && containsP(child, parent)) {
			line.setStroke(Color.BLUE);
			getChildren().remove(arrow);
		} else {
			line.setStroke(Color.RED);
		}

		line.startXProperty().bind(parent.layoutXProperty().add(parent.getBoundsInParent().getWidth() / 2.0));
		line.startYProperty().bind(parent.layoutYProperty().add(parent.getBoundsInParent().getHeight() / 2.0));

		line.endXProperty().bind(temp.layoutXProperty().add(temp.getBoundsInParent().getWidth() / 2.0));
		line.endYProperty().bind(temp.layoutYProperty().add(temp.getBoundsInParent().getHeight() / 2.0));

		weight.layoutXProperty().bind(parent.layoutXProperty().add(parent.getBoundsInParent().getWidth() / 4.0));
		weight.layoutYProperty().bind(parent.layoutYProperty().add(-5));

		setTextLayout(parent, temp);
		
		parent.layoutXProperty().addListener(e -> {
			setTextLayout(parent, temp);
			setArrowRotate(parent, temp);
		});

		parent.layoutYProperty().addListener(e -> {
			setTextLayout(parent, temp);
			setArrowRotate(parent, temp);
		});

		temp.layoutXProperty().addListener(e -> {
			setTextLayout(parent, temp);
			setArrowRotate(parent, temp);
		});

		temp.layoutYProperty().addListener(e -> {
			setTextLayout(parent, temp);
			setArrowRotate(parent, temp);
		});

	}

	private void setTextLayout(Node parent, Node child) {
		lineLength.layoutXProperty().bind(parent.layoutXProperty().add(parent.getBoundsInParent().getWidth() / 2.0 - lineLength.getBoundsInParent().getWidth() / 2.0 + (line.getEndX() - line.getStartX()) / 2));
		lineLength.layoutYProperty().bind(parent.layoutYProperty().add(parent.getBoundsInParent().getHeight() / 4.0 + lineLength.getBoundsInParent().getHeight() / 4.0 + (line.getEndY() - line.getStartY()) / 2));
		arrow.relocate(line.getStartX() - arrow.getLayoutBounds().getWidth() / 2, line.getStartY() - arrow.getLayoutBounds().getHeight() / 2);
		// lineLength.setText("" + getDiagonal(parent, child));
		// parent.linkLength.replace(child, getDiagonal(parent, child));
		// lineLength.setText("" + parent.linkLength.get(child));
		setArrowRotate(parent, child);
	}

	private boolean containsP(Link child, Node parent) {
		for (Link link : child.relatedNode.listLinks) {
			if (link.relatedNode == parent) {
				return true;
			}
		}
		return false;
	}

	private boolean isDown(Node parent, Node child) {
		if (parent.layoutY < child.layoutY) {
			return true;
		} else {
			return false;
		}
	}

	private boolean isRight(Node parent, Node child) {
		if (parent.layoutX < child.layoutX) {
			return true;
		} else {
			return false;
		}
	}

	private double calculateAngle(double a, double b) {
		return -1 * Math.atan(b / a);
	}

	private void setArrowRotate(Node parent, Node child) {
		double a;
		double b;
		double angle;
		if (isDown(parent, child)) {
			a = child.layoutY - parent.layoutY;
			if (isRight(parent, child)) {
				b = child.layoutX - parent.layoutX;
				angle = -1 * Math.toDegrees(calculateAngle(b, a)) - 90;
			} else {
				b = parent.layoutX - child.layoutX;
				angle = Math.toDegrees(calculateAngle(b, a)) + 90;
			}
			// arrow.setDown();
		} else {
			a = parent.layoutY - child.layoutY;
			if (isRight(parent, child)) {
				b = child.layoutX - parent.layoutX;
				angle = -1 * Math.toDegrees(calculateAngle(a, b)) + 180;
			} else {
				b = parent.layoutX - child.layoutX;
				angle = Math.toDegrees(calculateAngle(a, b)) - 180;
			}
			// arrow.setUp();
		}
		//System.out.println("a: " + a + " b: " + b);
		//System.out.println("angle: " + angle);
		arrow.setRotate(angle);
	}
}
