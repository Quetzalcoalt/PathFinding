package UI;

import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;

public class Arrow extends Group {
	Line main;
	Line left;
	Line right;

	public Arrow() {
		main = new Line();
		left = new Line();
		right = new Line();
		this.getChildren().addAll(main, left, right);
		setDown();
	}
	//TODO set start X and Y to Line start X and Y and end to be something so that i can be infront 
	public void setDown(){
		main.setStartX(0);
		main.setStartY(0);
		main.setEndX(0);
		main.setEndY(60);
		main.setStrokeWidth(0);
		left.setStartX(main.getEndX());
		left.setStartY(main.getEndY());
		left.setEndX(main.getEndX() - 8);
		left.setEndY(main.getEndY() - 8);
		left.setStrokeWidth(2);
		left.setStroke(Color.RED);
		right.setStartX(main.getEndX());
		right.setStartY(main.getEndY());
		right.setEndX(main.getEndX() + 8);
		right.setEndY(main.getEndY() - 8);
		right.setStrokeWidth(2);
		right.setStroke(Color.RED);
	}
	
	/*
	public void setUp(){
		main.setStartX(0);
		main.setStartY(0);
		main.setEndX(0);
		main.setEndY(-20);
		left.setStartX(main.getEndX());
		left.setStartY(main.getEndY());
		left.setEndX(main.getEndX() - 5);
		left.setEndY(main.getEndY() + 5);
		right.setStartX(main.getEndX());
		right.setStartY(main.getEndY());
		right.setEndX(main.getEndX() + 5);
		right.setEndY(main.getEndY() + 5);
	}
	*/
}
