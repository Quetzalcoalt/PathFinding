package UI;

import java.util.ArrayList;
import java.util.HashMap;

import AI.Graph;
import AI.GreedySearchByCoordinates;
import AI.Node;
import AI.SearchCheapestPath;
import AI.SearchShortestPath;
import AI.breathSearch;
import AI.ISearch;
import AI.Link;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Separator;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToolBar;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/* TODO make the one way arrows
 * check for duplicated connections 
*/
public class UI extends Application {

	double mouseX, mouseY;
	Graph graph;
	Pane window;
	ArrayList<Connection> connections = new ArrayList<>();
	static ArrayList<String> pathAR = new ArrayList<>();
	Group groupPathAnimation = new Group();
	ObservableList<String> options = FXCollections.observableArrayList("Breath First", "Greedy By Coordinates",
			"Shortest Path", "Cheapest Path");
	ArrayList<String> pointsToGoFirst = new ArrayList<>();
	static Boolean noPath = false;
	
	ToolBar toolbar;
	ToolBar secondToolBar;
	ToolBar toolbarBottom;

	Button newFile;
	Button save;
	Button open;
	Button searchB;
	Button addPoint;

	Label startLabel;
	Label endLabel;
	Label pathLabel;
	Label goThroughFirst;

	TextField startTF;
	TextField endTF;

	TextArea textArea;

	ComboBox<String> comboBoxPoints;
	ComboBox<String> searchAlgorithems;
	
	final ContextMenu contextMenuAddEditDelete = new ContextMenu();
	final ContextMenu contextMenuDelete = new ContextMenu();

	MenuItem edit;
	MenuItem delete;
	MenuItem add;

	public UI(Stage stage) {
		try {
			init();
			start(stage);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void aaa(Stage stage) {
		new UI(stage);
	}

	@Override
	public void start(Stage stage) throws Exception {

		graph = new Graph();

		BorderPane borderPane = new BorderPane();
		window = new Pane();
		borderPane.setCenter(window);

		toolbar = new ToolBar();
		secondToolBar = new ToolBar();
		toolbarBottom = new ToolBar();

		newFile = new Button("New");
		save = new Button("Save");
		open = new Button("Open");
		searchB = new Button("Search");
		addPoint = new Button("Add point");

		startLabel = new Label("Start from: ");
		endLabel = new Label("End here: ");
		pathLabel = new Label();
		goThroughFirst = new Label();

		startTF = new TextField();
		endTF = new TextField();

		textArea = new TextArea();
		textArea.setMaxWidth(400);
		textArea.setEditable(false);

		comboBoxPoints = new ComboBox<>();
		searchAlgorithems = new ComboBox<>(options);
		
		if (!searchAlgorithems.getItems().isEmpty()) {
			searchAlgorithems.setValue(searchAlgorithems.getItems().get(0));
		}

		edit = new MenuItem("Edit and delete cities");
		delete = new MenuItem("Delete selected city");
		add = new MenuItem("Add new city");

		contextMenuAddEditDelete.getItems().addAll(add, edit, delete);
		contextMenuDelete.getItems().add(delete);

		toolbar.getItems().addAll(newFile, save, open, new Separator(), searchAlgorithems, new Separator(), startLabel, startTF,
				endLabel, endTF, searchB);
		secondToolBar.getItems().addAll(new Label("Choose points to go through: "), comboBoxPoints, addPoint,
				goThroughFirst);
		toolbarBottom.getItems().add(pathLabel);

		borderPane.setTop(new VBox(toolbar, secondToolBar));
		borderPane.setBottom(toolbarBottom);
		borderPane.setRight(textArea);

		Scene scene = new Scene(borderPane, 1000, 600);

		scene.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent e) {
				if (e.getButton() == MouseButton.PRIMARY && e.getClickCount() == 2) {
					mouseX = e.getX();
					mouseY = e.getY() - toolbar.getHeight();
					contextMenuAddEditDelete.show(stage, e.getScreenX(), e.getScreenY());
				}
			}
		});

		scene.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent key) {
				if (key.getCode().equals(KeyCode.F5)) {
					reDraw();
					graph.resetAll();
					textArea.clear();
					pathLabel.setText("");
					if (window.getChildren().contains(groupPathAnimation)) {
						window.getChildren().remove(groupPathAnimation);
					}
				}
				if (key.getCode().equals(KeyCode.F2)) {
					graph.printAllLinks();
				}
			}
		});

		textArea.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent key) {
				if (key.getCode().equals(KeyCode.F5)) {
					reDraw();
					graph.resetAll();
					textArea.clear();
					pathLabel.setText("");
					if (window.getChildren().contains(groupPathAnimation)) {
						window.getChildren().remove(groupPathAnimation);
					}
				}
				if (key.getCode().equals(KeyCode.F2)) {
					graph.printAllLinks();
				}
			}
		});

		add.setOnAction(e -> {
			new AddCityDialog(graph).showAndWait().ifPresent((Node node) -> {
				if (node != null) {
					node.setLayoutPos(mouseX - node.circle.getRadius(), mouseY - node.circle.getRadius());
					graph.addNode(node);
					window.getChildren().add(node);
					graph.makeDragable(node);
					graph.makeClicable(node, contextMenuDelete, stage);
					comboBoxPoints.getItems().add(node.name);
					reDraw();
				}
			});
		});

		edit.setOnAction(e -> {
			new EditLinksDialog(graph).showAndWait().ifPresent((Boolean g) -> {
			});
			reDraw();
		});

		delete.setOnAction(e -> {
			Node node = graph.getNode(delete.getId());
			graph.deleteNode(node);
			comboBoxPoints.getItems().remove(node.name);
			window.getChildren().remove(node);
			reDraw();
		});
		
		newFile.setOnAction(e ->{
			graph.clearAll();
			reDraw();
		});

		save.setOnAction(e -> {
			SaveData.save(graph, stage);
		});

		open.setOnAction(e -> {
			OpenData.open(graph, stage);
			reDraw();
			graph.makeDragable();
			graph.makeClicable(contextMenuDelete, stage);
			fillComboBoxWithGraphPoints(graph, comboBoxPoints);
		});

		addPoint.setOnAction(e -> {
			pointsToGoFirst.add(comboBoxPoints.getSelectionModel().getSelectedItem());
			goThroughFirst
					.setText(goThroughFirst.getText() + comboBoxPoints.getSelectionModel().getSelectedItem() + ", ");
		});

		searchB.setOnAction(e -> {
			textArea.clear();
			switch (searchAlgorithems.getSelectionModel().getSelectedItem()) {
			case "Breath First":
				pointsToGoFirst.add(0, startTF.getText().toUpperCase());
				pointsToGoFirst.add(endTF.getText().toUpperCase());
				setTextToTextArea("Breath First", textArea);
				for (int i = 0; i < pointsToGoFirst.size() - 1; i++) {
					if(noPath){
						break;
					}
					search(new breathSearch(graph, textArea), pointsToGoFirst.get(i), pointsToGoFirst.get(i + 1), graph,
							pathLabel, textArea);
				}
				printPathToTextArea(pathLabel, textArea);
				startAnimation();
				resetSearch();
				break;
			case "Greedy By Coordinates":
				pointsToGoFirst.add(0, startTF.getText().toUpperCase());
				pointsToGoFirst.add(endTF.getText().toUpperCase());
				setTextToTextArea("Greedy By Coordinates", textArea);
				for (int i = 0; i < pointsToGoFirst.size() - 1; i++) {
					if(noPath){
						break;
					}
					search(new GreedySearchByCoordinates(graph, textArea), pointsToGoFirst.get(i),
							pointsToGoFirst.get(i + 1), graph, pathLabel, textArea);
				}
				printPathToTextArea(pathLabel, textArea);
				startAnimation();
				resetSearch();
				break;
			case "Shortest Path":
				pointsToGoFirst.add(0, startTF.getText().toUpperCase());
				pointsToGoFirst.add(endTF.getText().toUpperCase());
				setTextToTextArea("Shortest Path", textArea);
				for (int i = 0; i < pointsToGoFirst.size() - 1; i++) {
					if(noPath){
						break;
					}
					search(new SearchShortestPath(graph, textArea), pointsToGoFirst.get(i), pointsToGoFirst.get(i + 1),
							graph, pathLabel, textArea);
				}
				printPathToTextArea(pathLabel, textArea);
				startAnimation();
				resetSearch();
				break;

			case "Cheapest Path":
				pointsToGoFirst.add(0, startTF.getText().toUpperCase());
				pointsToGoFirst.add(endTF.getText().toUpperCase());
				setTextToTextArea("Cheapest Path", textArea);
				for (int i = 0; i < pointsToGoFirst.size() - 1; i++) {
					if(noPath){
						break;
					}
					search(new SearchCheapestPath(graph, textArea), pointsToGoFirst.get(i), pointsToGoFirst.get(i + 1),
							graph, pathLabel, textArea);
				}
				printPathToTextArea(pathLabel, textArea);
				startAnimation();
				resetSearch();
				break;
			}

		});

		stage.setTitle("Path finding");
		stage.setScene(scene);
		stage.show();

		init(graph);
		drawConnections();
		drawGraph();
		graph.makeDragable();
		graph.makeClicable(contextMenuDelete, stage);
		fillComboBoxWithGraphPoints(graph, comboBoxPoints);
	}

	private void setTextToTextArea(String string, TextArea textArea) {
		if (textArea.getText().equals("")) {
			textArea.setText(
					textArea.getText() + "New search started: " + string + "\nChosen cities: " + pointsToGoFirst);
		} else {
			textArea.setText(textArea.getText() + "\n\n" + "New search started: " + string + "\nChosen cities: "
					+ pointsToGoFirst);
		}

	}

	public static void init(Graph g) {
		Node A = new Node("A", 20);
		A.setLayoutPos(150, 150);
		Node B = new Node("B", 10);
		B.setLayoutPos(100, 200);
		Node C = new Node("C", 15);
		C.setLayoutPos(200, 200);
		Node D = new Node("D", 5);
		D.setLayoutPos(300, 300);
		Node E = new Node("E", 4);
		E.setLayoutPos(200, 300);
		Node F = new Node("F", 11);
		F.setLayoutPos(300, 200);

		g.addNode(A);
		g.addNode(B);
		g.addNode(C);
		g.addNode(D);
		g.addNode(E);
		g.addNode(F);

		g.twoWayLink("A", "B", 20, 0);
		g.twoWayLink("A", "C", 10, 1);
		g.twoWayLink("B", "D", 40, 2);
		g.twoWayLink("B", "F", 50, 0);
		g.twoWayLink("B", "E", 50, 1);
		g.twoWayLink("C", "F", 60, 0);
		g.oneWayLink("E", "F", 10, 1);
	}

	public static void search(ISearch s, String start, String end, Graph graph, Label pathLabel, TextArea textArea) {
		ArrayList<String> tempAR = new ArrayList<>();
		tempAR = s.hasPath(start, end);
		textArea.setText(textArea.getText() + "\n");
		if (tempAR == null) {
			textArea.setText(textArea.getText() + "\n" + "No path found!");
			pathLabel.setText("No path found!");
			noPath = true;
		} else {
			for (int i = tempAR.size() - 1; i >= 0; i--) {
				textArea.setText(textArea.getText() + tempAR.get(i) + " -> ");
				pathAR.add(tempAR.get(i));
			}
		}
	}
	
	private void resetSearch(){
		goThroughFirst.setText("");
		pointsToGoFirst.clear();
		pathAR.clear();
		noPath = false;
	}

	private void printPathToTextArea(Label pathLabel, TextArea textArea) {
		pathLabel.setText("Path is: ");
		textArea.setText(textArea.getText() + "\n" + "Full path is: ");
		for (int i = 0; i < pathAR.size(); i++) {
			graph.getNode(pathAR.get(i)).circle.setFill(Color.GREEN);
			pathLabel.setText(pathLabel.getText() + pathAR.get(i) + " -> ");
			textArea.setText(textArea.getText() + pathAR.get(i) + " -> ");
		}
	}

	private void startAnimation() {
		if (window.getChildren().contains(groupPathAnimation)) {
			window.getChildren().remove(groupPathAnimation);
		}
		if (pathAR != null) {
			groupPathAnimation = PathAnimation.startAnimation(pathAR, graph);
			window.getChildren().add(groupPathAnimation);
		}
	}

	private void reDraw() {
		window.getChildren().clear();
		drawConnections();
		drawGraph();
	}

	private void drawGraph() {
		HashMap<String, Node> map = graph.getMap();

		for (Node node : map.values()) {
			window.getChildren().add(node);
		}
	}

	private void drawConnections() {
		connections.clear();

		for (Node parent : graph.getMap().values()) {
			for (Link child : parent.listLinks) {
				connections.add(new Connection(parent, child));
			}
		}
		window.getChildren().addAll(connections);
	}

	private void fillComboBoxWithGraphPoints(Graph graph, ComboBox<String> cb) {
		cb.getItems().clear();
		for (Node node : graph.getMap().values()) {
			cb.getItems().add(node.name);
		}
		cb.getSelectionModel().select(0);
	}

}
