package UI;

import AI.Graph;
import AI.Node;
import javafx.event.EventHandler;
//import javafx.scene.Node;
import javafx.scene.control.ContextMenu;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class MouseGestures {

	Graph graph;
	double x;
	double y;
	ContextMenu menu;
	Stage stage;

	public MouseGestures(Graph graph) {
		this.graph = graph;
	}

	public void makeDraggable(final Node node) {
		node.setOnMousePressed(onMousePressedEventHandler);
		node.setOnMouseDragged(onMouseDraggedEventHandler);
	}

	public void makeClickable(Node node, ContextMenu menu, Stage stage) {
		node.setOnMouseClicked(onMouseClickedEventHandler);
		this.menu = menu;
		this.stage = stage;
	}

	EventHandler<MouseEvent> onMouseClickedEventHandler = new EventHandler<MouseEvent>() {

		@Override
		public void handle(MouseEvent event) {
			if (event.getButton() == MouseButton.SECONDARY) {
				Node node = (Node) event.getSource();
				menu.show(stage, event.getScreenX(), event.getScreenY());
				menu.getItems().get(0).setId(node.name);
			}
		}
	};

	EventHandler<MouseEvent> onMousePressedEventHandler = new EventHandler<MouseEvent>() {

		@Override
		public void handle(MouseEvent event) {

			Node node = (Node) event.getSource();

			x = node.getBoundsInParent().getMinX() - event.getScreenX();
			y = node.getBoundsInParent().getMinY() - event.getScreenY();
		}
	};

	EventHandler<MouseEvent> onMouseDraggedEventHandler = new EventHandler<MouseEvent>() {

		@Override
		public void handle(MouseEvent event) {
			Node node = (Node) event.getSource();

			double offsetX = event.getScreenX() + x;
			double offsetY = event.getScreenY() + y;
			node.relocate(offsetX, offsetY);
			node.setLayoutPos(offsetX, offsetY);
			node.x = offsetX;
			node.y = offsetY;
		}
	};
	
}
