package UI;

import java.util.ArrayList;

import AI.Graph;
import AI.Node;
import javafx.animation.PathTransition;
import javafx.animation.Timeline;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.util.Duration;

public class PathAnimation extends Group {

	public static Group startAnimation(ArrayList<String> ar, Graph graph) {
		final Circle circlePath = new Circle(10, Color.ORANGE);
		Path path = new Path();
		boolean isMoveTo = false;
		double ms = 0;

		for (int i = 0; i < ar.size(); i++) {
			if (!isMoveTo) {
				Node node = graph.getNode(ar.get(i));
				double x = node.layoutX + node.circle.getRadius();
				double y = node.layoutY + node.circle.getRadius();
				path.getElements().add(new MoveTo(x,y));
				isMoveTo = true;
				ms = 1000;
			} else {
				Node node = graph.getNode(ar.get(i));
				double x = node.layoutX + node.circle.getRadius();
				double y = node.layoutY + node.circle.getRadius();
				path.getElements().add(new LineTo(x, y));
				ms += 1000;
			}
		}

		PathTransition pathTransition = new PathTransition();
		pathTransition.setDuration(Duration.millis(ms));
		pathTransition.setPath(path);
		pathTransition.setNode(circlePath);
		pathTransition.setOrientation(PathTransition.OrientationType.ORTHOGONAL_TO_TANGENT);
		pathTransition.setCycleCount(Timeline.INDEFINITE);
		pathTransition.setAutoReverse(false);
		pathTransition.play();
		Group group = new Group();
		group.getChildren().addAll(circlePath, path);
		return group;
	}

}
