package UI;

import java.util.ArrayList;

import AI.Graph;
import AI.Node;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;

public class stuff {

	/* Tova razdelq vavedenite imena na zapetaqra, maha vsi4ki special simvuli i space-ove + pravi vsi4ko uppercase i gi slava v arrayList<String>
	 * ako moje da se napravi vseki sa6testvuva6 element da se svarzva 
	if (param == buttonTypeAdd) {
		String[] words1 = linksTextField.getText().split(",");
		ArrayList<String> words = new ArrayList<>();
		
		String mainCity = cityNameTextField.getText().toUpperCase();
		mainCity = mainCity.replaceAll("\\s+","");
		
		//Makes all the words without spaces and UPPER cases
		for(String word: words1){
			word = word.toUpperCase();
			word = word.replaceAll("\\s+","");
			if(word.equals(mainCity)){
				word = "";
			}
			if(!word.equals("")){
				words.add(word);
			}
		}
		
		//Deletes the duplicates
		for(int i = 0; i < words.size(); i++){
			for(int j = i + 1; j < words.size(); j++){
				if(words.get(i).equals(words.get(j))){
					words.remove(words.get(i));
					i--;
				}
			}
		}
		*/
	
	
	/*-------------FROM EDIT BUTTON
	ArrayList<String> addOneWayLinks = new ArrayList<>();
	ArrayList<String> addTwoWayLinks = new ArrayList<>();
	ArrayList<String> removeOneWayLinks = new ArrayList<>();
	ArrayList<String> removeTwoWayLinks = new ArrayList<>();

	Label LabelAddOneWayLinks = new Label("");
	Label LabelAddTwoWayLinks = new Label("");
	Label LabelRemoveOneWayLinks = new Label("");
	Label LabelRemoveTwoWayLinks = new Label("");
	Label oneWay = new Label("Add one way links!");
	Label twoWay = new Label("Add two way links!");
	Label deleteOneWay = new Label("Delete one way existing links!");
	Label deleteTwoWay = new Label("Delete two way existing links!");
	
	ComboBox<String> comboBoxAddOneLinks = new ComboBox<>();
	ComboBox<String> comboBoxAddTwoLinks = new ComboBox<>();
	ComboBox<String> comboBoxRemoveOneWayLinks = new ComboBox<>();
	ComboBox<String> comboBoxRemoveTwoWayLinks = new ComboBox<>();
	
	Button addOneWay = new Button("Add");
	Button addTwoWay = new Button("Add");
	Button removeOneWay = new Button("Remove");
	Button removeTwoWay = new Button("Remove");
	
	fillLinksCB(graph, node, comboBoxAddOneLinks);	
	fillLinksCB(graph, node, comboBoxAddTwoLinks);
	fillRemoveLinksCB(graph, node, comboBoxRemoveOneWayLinks);
	fillRemoveLinksCB(graph, node, comboBoxRemoveTwoWayLinks);
	
	GridPane grid = new GridPane();
	grid.add(oneWay, 1, 1);
	grid.add(new HBox(comboBoxAddOneLinks, addOneWay), 1, 2);
	grid.add(LabelAddOneWayLinks, 1, 3);
	grid.add(twoWay, 1, 4);
	grid.add(new HBox(comboBoxAddTwoLinks, addTwoWay), 1, 5);
	grid.add(LabelAddTwoWayLinks, 1, 6);
	grid.add(deleteOneWay, 1, 7);
	grid.add(new HBox(comboBoxRemoveOneWayLinks, removeOneWay), 1, 8);
	grid.add(LabelRemoveOneWayLinks, 1, 9);
	grid.add(deleteTwoWay, 1, 10);
	grid.add(new HBox(comboBoxRemoveTwoWayLinks, removeTwoWay), 1, 11);
	grid.add(LabelRemoveTwoWayLinks, 1, 12);
	
	DialogPane dialog = getDialogPane();
	setTitle("Add links to the city");
	dialog.setContent(grid);

	ButtonType buttonTypeApply = new ButtonType("Apply", ButtonData.OK_DONE);
	ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
	dialog.getButtonTypes().addAll(buttonTypeApply, buttonTypeCancel);

	setResultConverter(dialogButton -> {
		final ButtonData data = dialogButton.getButtonData();
		if (data == ButtonData.OK_DONE) {
			for (String string : addOneWayLinks) {
				graph.oneWayLink(node.name, string, 0, 0);
			}
			for (String string : addTwoWayLinks) {
				graph.twoWayLink(node.name, string, 0, 0);
			}
			for (String string : removeOneWayLinks) {
				node.links.remove(graph.getNode(string));
			}
			for (String string : removeTwoWayLinks) {
				node.links.remove(graph.getNode(string));
				graph.getNode(string).links.remove(node);
			}
			return true;
		} else {
			return false;
		}
	});

	addOneWay.setOnAction(new EventHandler<ActionEvent>() {
		public void handle(ActionEvent event) {
			if (!comboBoxAddOneLinks.getItems().isEmpty()) {
				addOneWayLinks.add(comboBoxAddOneLinks.getValue());
				comboBoxAddTwoLinks.getItems().remove(comboBoxAddOneLinks.getValue());
				comboBoxAddOneLinks.getItems().remove(comboBoxAddOneLinks.getValue());
				if(!comboBoxAddOneLinks.getItems().isEmpty()){
					comboBoxAddOneLinks.setValue(comboBoxAddOneLinks.getItems().get(0));
				}
				if(!comboBoxAddTwoLinks.getItems().isEmpty()){
					comboBoxAddTwoLinks.setValue(comboBoxAddTwoLinks.getItems().get(0));
				}
			}

			LabelAddOneWayLinks.setText(addOneWayLinks.get(0));
			for (int i = 1; i < addOneWayLinks.size(); i++) {
				LabelAddOneWayLinks.setText(LabelAddOneWayLinks.getText() + ", " + addOneWayLinks.get(i));
			}
		}
	});
	
	addTwoWay.setOnAction(new EventHandler<ActionEvent>() {
		public void handle(ActionEvent event) {
			if (!comboBoxAddTwoLinks.getItems().isEmpty()) {
				addTwoWayLinks.add(comboBoxAddTwoLinks.getValue());
				comboBoxAddOneLinks.getItems().remove(comboBoxAddTwoLinks.getValue());
				comboBoxAddTwoLinks.getItems().remove(comboBoxAddTwoLinks.getValue());
				if(!comboBoxAddTwoLinks.getItems().isEmpty()){
					comboBoxAddTwoLinks.setValue(comboBoxAddTwoLinks.getItems().get(0));
				}
				if(!comboBoxAddOneLinks.getItems().isEmpty()){
					comboBoxAddOneLinks.setValue(comboBoxAddOneLinks.getItems().get(0));
				}
			}

			LabelAddTwoWayLinks.setText(addTwoWayLinks.get(0));
			for (int i = 1; i < addTwoWayLinks.size(); i++) {
				LabelAddTwoWayLinks.setText(LabelAddTwoWayLinks.getText() + ", " + addTwoWayLinks.get(i));
			}
		}
	});

	removeOneWay.setOnAction(new EventHandler<ActionEvent>() {
		public void handle(ActionEvent event) {
			if (!comboBoxRemoveOneWayLinks.getItems().isEmpty()) {
				removeOneWayLinks.add(comboBoxRemoveOneWayLinks.getValue());
				comboBoxRemoveTwoWayLinks.getItems().remove(comboBoxRemoveOneWayLinks.getValue());
				comboBoxRemoveOneWayLinks.getItems().remove(comboBoxRemoveOneWayLinks.getValue());
				if(!comboBoxRemoveOneWayLinks.getItems().isEmpty()){
					comboBoxRemoveOneWayLinks.setValue(comboBoxRemoveOneWayLinks.getItems().get(0));
				}
				if(!comboBoxRemoveTwoWayLinks.getItems().isEmpty()){
					comboBoxRemoveTwoWayLinks.setValue(comboBoxRemoveTwoWayLinks.getItems().get(0));
				}
			}

			LabelRemoveOneWayLinks.setText(removeOneWayLinks.get(0));
			for (int i = 1; i < removeOneWayLinks.size(); i++) {
				LabelRemoveOneWayLinks.setText(LabelRemoveOneWayLinks.getText() + ", " + removeOneWayLinks.get(i));
			}
		}
	});
	
	removeTwoWay.setOnAction(new EventHandler<ActionEvent>() {
		public void handle(ActionEvent event) {
			if (!comboBoxRemoveTwoWayLinks.getItems().isEmpty()) {
				removeTwoWayLinks.add(comboBoxRemoveTwoWayLinks.getValue());
				comboBoxRemoveOneWayLinks.getItems().remove(comboBoxRemoveTwoWayLinks.getValue());
				comboBoxRemoveTwoWayLinks.getItems().remove(comboBoxRemoveTwoWayLinks.getValue());
				if(!comboBoxRemoveTwoWayLinks.getItems().isEmpty()){
					comboBoxRemoveTwoWayLinks.setValue(comboBoxRemoveTwoWayLinks.getItems().get(0));
				}
				if(!comboBoxRemoveOneWayLinks.getItems().isEmpty()){
					comboBoxRemoveOneWayLinks.setValue(comboBoxRemoveOneWayLinks.getItems().get(0));
				}
			}

			LabelRemoveTwoWayLinks.setText(removeTwoWayLinks.get(0));
			for (int i = 1; i < removeTwoWayLinks.size(); i++) {
				LabelRemoveTwoWayLinks.setText(LabelRemoveTwoWayLinks.getText() + ", " + removeTwoWayLinks.get(i));
			}
		}
	});
}

public void fillLinksCB(Graph graph, Node node, ComboBox<String> combobox){
	for (Node nodes : graph.getMap().values()) {
		combobox.getItems().add(nodes.name);
	}

	for (Node link : node.links) {
		for (Node nodes : graph.getMap().values()) {
			if (link == nodes) {
				combobox.getItems().remove(nodes.name);
			}
		}
	}
	combobox.getItems().remove(node.name);
	if(!combobox.getItems().isEmpty()){
		combobox.setValue(combobox.getItems().get(0));
	}
}

public void fillRemoveLinksCB(Graph graph, Node node, ComboBox<String> combobox){
	for (Node link : node.links) {
		combobox.getItems().add(link.name);
	}
	if(!combobox.getItems().isEmpty()){
		combobox.setValue(combobox.getItems().get(0));
	}
}
*/
	
	
}
